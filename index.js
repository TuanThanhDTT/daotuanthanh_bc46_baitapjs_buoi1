/**
 * Bài 1
 * Đầu vào: lương 1 ngày (100.000) và nhập số ngày làm.
 * Các bước xử lí: 
 * + Bước 1 : Tạo 1 biến gán lương cơ bản là 100.000
 * + Bước 2 : Tạo 1 biến gán một số cho số ngày làm việc
 * + Bước 3 : Tạo 1 biến nhận giá trị là tổng lương = lương cơ bản * số ngày làm việc
 * + Bước 4 : In ra console giá trị biến tongLuong
 * Đầu ra: tổng lương được nhận.
 */
var luongCoBan = 100000.00;
var soNgayLam = 30;
var tongLuong = luongCoBan * soNgayLam;
console.log("Tổng lương là:",tongLuong);

/**
 * Bài 2:
 * Đầu vào: nhập 5 số thực bất kì
 * Các bước xử lí:
 * + Bước 1: Khai báo 5 biến lần lượt là 5 số
 * + Bước 2: Khai báo biến trungBinhCong để nhận giá trị là trungBinhCong = (Tổng 5 số)/5
 * + Bước 3: In ra console giá trị biến trungBinhCong
 * Đầu ra: Giá trị trung bình của 5 số
 */
var num1 = 10;
var num2 = 20;
var num3 = 30;
var num4 = 40;
var num5 = 50;
var trungBinhCong = (num1 + num2 + num3 + num4 + num5) / 5;
console.log("Trung bình 5 số là:",trungBinhCong);

/**
 * Bài 3:
 * Đầu vào: giá trị 1USD = 23.500VND, số USD được nhập.
 * Các bước xử lí:
 * + Bước 1: Khởi tạo 1 biến USD = 23.500
 * + Bước 2: Khởi tạo 1 biến numUSD là số USD được nhập bất kì.
 * + Bước 3: Tạo 1 biến là VND chứa giá trị VND = (USD * numUSD)
 * + Bước 4: In giá trị ra console
 * Đầu ra: Tính và xuất ra giá trị USD quy đổi sang VND.
 */
var USD = 23500.00;
var numUSD = 10;
var VND = (USD * numUSD);
console.log("Số tiền USD đổi sang VND là:", VND);

/**
 * Bài 4:
 * Đầu vào: nhập hai giá trị chiều dài, chiều rộng của hình chữ nhật
 * Các bước xử lí:
 * + Bước 1: Tạo 1 biến chieuDai và gán giá trị cho chiều dài 
 * + Bước 2: Tạo 1 biến chieuRong gán giá trị cho chiều rộng 
 * + Bước 3: Tạo 1 biến chuVi nhận giá trị phép tính chuVi = (chieuDai + chieuRong) / 2
 * + Bước 4: Tạo 1 biến dienTich nhận giá trị phép tính dienTich = (chieuDai * chieuRong);
 * + Bước 5: In giá trị ra console
 * Đầu ra: Diện tích và Chu vi hình Chữ Nhật
 */
var chieuDai = 30;
var chieuRong = 25;
var chuVi = (chieuDai + chieuRong) / 2;
var dienTich = (chieuDai * chieuRong);
console.log("Chu Vi hình Chữ Nhât là:",chuVi);
console.log("Diện tích hình Chữ Nhât là:",dienTich);
 /**
  * Bài 5:
  * Đầu vào: nhập vào một số gồm 2 ký số (ví dụ 25)
  * Các bước xử lí:
  * + Bước 1: Tạo 1 biến là num chứa một số gồm hai ký số
  * + Bước 2: Tạo 1 biến là numDonVi để lấy số hàng đơn vị
  * + Bước 3: Tạo 1 biến là numHangChuc để lấy số hàng chục
  * + Bước 4: Tạo 1 biến là tongHaiKySo để thực hiện phép tính  var tongHaiKySo = numDonVi + numHangChuc;
  * + Bước 5: In giá trị ra console
  * Đầu ra: tổng hai ký số vừa nhập
  */
 var num = 25;
 var numDonVi = num % 10;
 var numHangChuc = (num - numDonVi) / 10;
 var tongHaiKySo = numDonVi + numHangChuc;
 console.log("Tổng hai ký số là:",tongHaiKySo);

